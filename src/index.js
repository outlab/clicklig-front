import React from 'react';
import ReactDOM from 'react-dom';

import Routes from './services/routes';

import './assets/css/global.css';
import './assets/css/mobile.css';

ReactDOM.render(
  <Routes />,
  document.getElementById('root')
);