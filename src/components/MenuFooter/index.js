import React, { useState } from "react";
import { Link } from "react-router-dom";

import './menufooter.css'

const MenuFooter = () => {

    return (
  
      <>
  
        <div className="menu-footer">
            <ul>
                <li><Link to="/">Home</Link></li>
                <li><Link to="/fale-conosco">Contato</Link></li>
                <li><Link to="/faq">FAQ</Link></li>
                <li><Link to="/politica-de-privacidade">Política de Privacidade</Link></li>
                <li><Link to="/termos-e-condicoes">Termos e Condições</Link></li>
            </ul>
        </div>
  
      </>
    );
  }
  
  export default MenuFooter;