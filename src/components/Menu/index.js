import React, { useState } from "react";
import { Link } from "react-router-dom";

import './menu.css'

const Menu = () => {

    return (
  
      <>
  
        <nav id="sidemenu">
            <ul>
                <li><Link to="/">Home</Link></li>
                <li><Link to="/fazer-pedido">Fazer Pedido</Link></li>
                <li><Link to="/seja-um-fornecedor">Seja um Fornecedor</Link></li>
                <li><Link to="/fale-conosco">Contato</Link></li>
            </ul>
        </nav>

      </>
    );
  }
  
  export default Menu;