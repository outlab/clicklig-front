import React from "react";

import './style.css';

import bannerCriarconta from "../../assets/images/criar-conta.png";
import CreateAccountForm from "../CreateAccountForm";

const CreateAccount = () => {

  return (

    <> 

<div class="createaccount">

    <div class="imagem-pre-footer"><img src={bannerCriarconta} /></div>

      <CreateAccountForm />

      </div>

    </>

  );
}

export default CreateAccount;