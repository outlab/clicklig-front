import React, { useState } from "react";
import { useForm } from "react-hook-form";

import Axios from "axios";
import { useHistory, Link } from "react-router-dom";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


const CreateAccountForm = () => {

  const [name, setUserName] = useState('');
  const [cpf, setUserCpf] = useState('');
  const [email, setUserEmail] = useState('');
  const [password, setUserPassword] = useState('');

  const { handleSubmit } = useForm();

  const history = useHistory();

  function registerForm() {
      
    Axios.post( process.env.REACT_APP_API_BASE_URL + "/auth/register", {
      name: name,
      cpf: cpf,
      email: email,
      password: password,
      role: 'client'
    })
    .then((res) => {
      localStorage.setItem('token', JSON.stringify(res.data.token));
      //Axios.defaults.headers.Authorization = `Bearer ${res.data.token}`;
    })
    .then((res) => {
      history.push("/meus-dados");
    })
    .catch((error) => {
        console.log(error.response.data.error);
        toast.error('😲' + error.response.data.error)
    });

  };

  return (

    <> 

      <div class="form-createaccount">

        <h3>Criar sua conta</h3>

        <span class="destaque"><p><span>Já tem uma conta?</span> <span><Link to="login">Entrar</Link></span></p></span>

        <form class="form-criar-conta" onSubmit={handleSubmit(registerForm)} id="registerform">

            <label htmlFor="name">Nome Completo</label>
            <input type="text" id="name" name="name" placeholder="" onChange={e => setUserName(e.target.value)} />

            <label htmlFor="cpf">CPF ou CNPJ</label>
            <input type="text" id="cpf" name="cpf" onChange={e => setUserCpf(e.target.value)} />

            <label htmlFor="email">E-mail</label>
            <input type="text" id="email" name="email" onChange={e => setUserEmail(e.target.value)} />

            <label htmlFor="password">Senha</label>
            <input type="password" id="password" name="password" onChange={e => setUserPassword(e.target.value)} />

            <label for="ciente" class="ciente"><input type="checkbox" id="ciente" name="ciente" /> Estou ciente dos <Link to="/termos-e-condicoes" target="_blank">Termos e Condições</Link></label>

              <p class="aviso">Seus dados serão usados para aprimorar a sua experiência em todo este site, para gerenciar o acesso a sua conta e para outros propósitos, como descritos em nossa política de privacidade.</p>

            <button class="btn">Criar</button>

        </form>

      </div>

      <ToastContainer />

    </>

  );
}

export default CreateAccountForm;