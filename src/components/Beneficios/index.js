import React, { useState } from "react";

import { faCheckCircle } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";



import './beneficios.css'

const Beneficios = () => {
  

    return (
  
      <>
         
    <div class="line">
      <div id="barra"></div>
    </div>

    <section class="beneficios">

      <div class="beneficiobox" id="one">

        <i class="fas"><FontAwesomeIcon icon={faCheckCircle} /></i>

        <h3>Garantia de qualidade e menor preço.</h3>

      </div>

      <div class="beneficiobox" id="two">

      <i class="fas"><FontAwesomeIcon icon={faCheckCircle} /></i>

        <h3>Compre tudo sem sair de casa em um só lugar.</h3>

      </div>

      <div class="beneficiobox" id="three">

      <i class="fas"><FontAwesomeIcon icon={faCheckCircle} /></i>

        <h3>Pague com cartão de crédito, boleto, Pix ou débito.</h3>

      </div>

      <div class="beneficiobox" id="four">

      <i class="fas"><FontAwesomeIcon icon={faCheckCircle} /></i>

        <h3>Esteja conectado com fornecedores da sua região.</h3>

      </div>

    </section>
  
      </>
    );
  }
  
  export default Beneficios;