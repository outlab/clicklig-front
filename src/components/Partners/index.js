import React, { useState } from "react";
import Slider from "react-slick";

import parceiroImg from "../../assets/images/parceiro.png";

import './style.css'

const Partners = () => {  

  const centerSlide = {
    //className: "center",
    //centerMode: true,
    autoPlay: true,
    infinite: true,
    centerPadding: "60px",
    slidesToShow: 5,
    speed: 500,
    arrows: false,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          initialSlide: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        }
      }
    ]
  };

  return (

    <>

      
        <ul class="logo-parceiros">
          <Slider {...centerSlide}>
            <li><img src={parceiroImg} /></li>
            <li><img src={parceiroImg} /></li>
            <li><img src={parceiroImg} /></li>
            <li><img src={parceiroImg} /></li>
            <li><img src={parceiroImg} /></li>
            <li><img src={parceiroImg} /></li>
            <li><img src={parceiroImg} /></li>
            <li><img src={parceiroImg} /></li>
            <li><img src={parceiroImg} /></li>
          </Slider>
        </ul>
      

    </>

  );

}
  
export default Partners;