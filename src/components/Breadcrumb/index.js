import React, { useEffect, useState } from "react";
import { useHistory, Link, useLocation } from "react-router-dom";
import Axios from 'axios';

import { faHome, faAngleRight } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

//import './header.css'

const Breadcrumb = () => {

  const location = useLocation();
  const urlpath = location.pathname.split('-').join(' ').replaceAll("/", "");
  //const urlname = urlpath.split('-').join(' ')
  
  return (

    <>

      <div class="path">

        <ul>
          <li>
            <Link to="/"><i class="fas"><FontAwesomeIcon icon={faHome} /></i> Home</Link>
          </li>
          <li><i class="fas"><FontAwesomeIcon icon={faAngleRight} /></i></li>
          <li>{urlpath}</li>
        </ul>

      </div>
    
    </>
  );
}

export default Breadcrumb;