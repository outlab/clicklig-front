import React, { useEffect, useState } from "react";
import { useHistory, Link } from "react-router-dom";
import Axios from 'axios';
import { useMediaQuery } from 'react-responsive';
import Whatsapp from '../Whatsapp';

import Menu from '../Menu';

import { faBars } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import './header.css'

import logoHeader from "../../assets/images/logo.png";
import logoMobile from "../../assets/images/logo-mobile.png";

const Header = () => {

  const isDesktopOrLaptop = useMediaQuery({
    query: '(min-device-width: 1024px)'
  });
  const isTabletOrMobile = useMediaQuery({ query: '(max-width: 480px)' });

  function openMenu() {
    var openmenu = document.getElementById("sidemenu");
    openmenu.classList.toggle("active");
  }

  const [user, setUser] = useState([]);

  const history = useHistory();

  function clearStorage() { 
    localStorage.clear();
    history.push("/login");
  }

  useEffect(() => {

    const token = JSON.parse(localStorage.getItem('token'));

    const fetchUser = async () => {
      const result = await Axios.get( process.env.REACT_APP_API_BASE_URL + "/users/user", {
        headers: {
          'authorization': 'Bearer ' + token
        }
      });
      setUser(result.data);
    };
 
    fetchUser();

  },[]);

  return (

    <>

    <header>

      <div class="header">

      {isTabletOrMobile && <>
       
          <FontAwesomeIcon icon={faBars} className="menumobile" onClick={openMenu} />
      
      </>}

      <div class="logo"><Link to="/"><img src={logoHeader} /></Link></div>

      <div class="logo-mobile"><Link to="/"><img src={logoMobile} /></Link></div>

        <div>

          <Menu />

          <div class="btns">

              {user.name ? (
                <ul>

                  <li><Link to="/meus-dados"><span className="useraccount">Olá, {user.name.split(" ")[0]}!</span></Link></li>

                  <li><span className="exit" onClick={clearStorage}>Sair</span></li>

                </ul>
              ) : 
              
              <ul>

                  <li><Link to="login"><button class="btn">Entrar</button></Link></li>

                  <li><Link to="criar-conta"><button class="btn border">Cadastrar</button></Link></li>

              </ul> 
              
              }

          </div>

        </div>

      

      </div>

    </header>
    <Whatsapp />
    </>
  );
}

export default Header;