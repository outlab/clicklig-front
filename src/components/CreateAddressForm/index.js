import React, { useState } from "react";
import { useForm } from "react-hook-form";

import Axios from "axios";
import { useHistory, Link } from "react-router-dom";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import './style.css';

const CreateAddressForm = () => {

  const [name, setAddressName] = useState('');
  const [cep, setAddressCep] = useState('');
  const [street, setAddressStreet] = useState('');
  const [number, setAddressNumber] = useState('');
  const [complement, setAddressComplement] = useState('');
  const [district, setAddressDistrict] = useState('');
  const [city, setAddressCity] = useState('');
  const [state, setAddressState] = useState('');

  const { handleSubmit } = useForm();

  const history = useHistory();

  function addressForm() {
      
    const token = JSON.parse(localStorage.getItem('token'));

      let axiosConfig = {
        headers: {
          'authorization': 'Bearer ' + token
        }
      };
    
      const address = new FormData();
      address.append('name', name);
      address.append('cep', cep);
      address.append('street', street);
      address.append('complement', complement);
      address.append('district', district);
      address.append('city', city);
      address.append('state', state);
      
      Axios.post( process.env.REACT_APP_API_BASE_URL + "/address/new", {
        name: name,
        cep: cep,
        street: street,
        number: number,
        complement: complement,
        district: district,
        city: city,
        state: state
      }, axiosConfig)
      .then((res) => {
        toast.success('O endereço foi cadastrado com sucesso.')
        window.location.reload();
      })
      .catch((error) => {
          console.log(error.response.data.error);
          toast.error('😲' + error.response.data.error)
      });

  };

  return (

    <> 

      <div class="form-createaddress">

        <h2 className="linetitle"><span>Adicionar um endereço</span></h2>

        <form onSubmit={handleSubmit(addressForm)} id="addressform">
            
            <section className="formgrid">

              <div>
                <label htmlFor="name">Nome do Endereço</label>
                <input type="text" id="name" name="name" placeholder="" onChange={e => setAddressName(e.target.value)} />
              </div>

              <div>
                <label htmlFor="cep">CEP</label>
                <input type="text" id="cep" name="cep" placeholder="" onChange={e => setAddressCep(e.target.value)} />
              </div>

              <div>
                <label htmlFor="street">Endereço</label>
                <input type="text" id="street" name="street" onChange={e => setAddressStreet(e.target.value)} />
              </div>

              <div>
                <label htmlFor="number">Número</label>
                <input type="text" id="number" name="number" onChange={e => setAddressNumber(e.target.value)} />
              </div>

              <div>
              <label htmlFor="complement">Complemento</label>
                <input type="text" id="complement" name="complement" onChange={e => setAddressComplement(e.target.value)} />
              </div>

              <div>
                <label htmlFor="district">Bairro</label>
                <input type="text" id="district" name="district" onChange={e => setAddressDistrict(e.target.value)} />
              </div>

              <div>
                <label htmlFor="city">Cidade</label>
                <input type="text" id="city" name="city" onChange={e => setAddressCity(e.target.value)} />
              </div>

              <div>
                <label htmlFor="state">Estado</label>
                <input type="text" id="state" name="state" onChange={e => setAddressState(e.target.value)} />
              </div>
            </section>

            <div></div><div></div>

            <button class="btn">Adicionar Novo Endereço</button>

            <div className="clear"></div>

        </form>

      </div>

      <ToastContainer />

    </>

  );
}

export default CreateAddressForm;