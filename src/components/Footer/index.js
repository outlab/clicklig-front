import React, { useState } from "react";

import MenuFooter from '../MenuFooter';

import './footer.css'

import logoFooter from "../../assets/images/logofooter.png";

const Footer = () => {

  return (

    <>

      <footer>

        <div class="footer-top">

            <div class="logo-footer"><a href="#"><img src={logoFooter} /></a></div>

            <div class="menu_footer"> <MenuFooter /></div>

        </div>

        <div class="rights"><p>ClickLig - Todos os direitos reservados - 2021</p></div>

      </footer>

    </>
  );
}

export default Footer;