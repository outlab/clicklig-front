import React, { useState } from "react";
import { Link } from "react-router-dom";

import { faWhatsapp } from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";


import './whatsapp.css';

const Whatsapp = () => {  


  return (

    <>
      <a class="link-whats" target="_blank" href="https://api.whatsapp.com/send?phone=5521970954514&text=Ol%C3%A1!%20Acessei%20o%20site%20ClickLig%20e%20gostaria%20de%20mais%20informa%C3%A7%C3%B5es.">
            <div class="whatsapp">
            <span><i class="fas"><FontAwesomeIcon icon={faWhatsapp} /></i><span class="text">Clique aqui e fale com nossa equipe de especialistas. | (21) 97095-4514</span></span>
            </div>
      </a>

    </>

  );

}
  
export default Whatsapp;