import React, { useEffect, useState } from "react";
import Axios from 'axios';
import { useHistory, Link } from "react-router-dom";

import { faUser, faMapMarkerAlt, faBoxOpen} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import barraTitle from "../../assets/images/barra-title.svg";
import Linha from "../../assets/images/etapaum.png";

//import './style.css';

const UserSideMenu = () => {

  const [user, setUser] = useState([]);

    useEffect(() => {

        const token = JSON.parse(localStorage.getItem('token'));

        const fetchUser = async () => {
            const result = await Axios.get( process.env.REACT_APP_API_BASE_URL + "/users/user", {
            headers: {
                'authorization': 'Bearer ' + token
            }
            });
            setUser(result.data);
        };
        
        fetchUser();

    },[]);

  return (

    <> 

      <div className="left">

        <h1 className="brandtitle">Olá, {user.name}</h1>

        <ul>
            <li className="active"><Link to="meus-dados"><i className="fas"><FontAwesomeIcon icon={faUser} /></i>Meus Dados</Link></li>
            <li><Link to="meus-enderecos"><i className="fas"><FontAwesomeIcon icon={faMapMarkerAlt} /></i>Endereços</Link></li>
            <li><Link to="meus-pedidos"><i className="fas"><FontAwesomeIcon icon={faBoxOpen} /></i>Meus Pedidos</Link></li>
        </ul>

      </div>

    </>

  );
}

export default UserSideMenu;