import React, { useState,useEffect } from "react";
import { useHistory, Link } from "react-router-dom";
import Axios from 'axios';

import Header from '../../components/Header';
import Footer from '../../components/Footer';
import Breadcrumb from "../../components/Breadcrumb";

import { faHome} from "@fortawesome/free-solid-svg-icons";
import { faAngleRight} from "@fortawesome/free-solid-svg-icons";
import { faCheckCircle } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import barraTitle from "../../assets/images/barra-title.svg";
import imgPedido from "../../assets/images/pedido.png"

import './pedido.css';

const PedidoConfirmado = () => {

    const [user, setUser] = useState([]);
    const [lenght, setOrderLenght] = useState('');

    const total_lenght = lenght.length;

    useEffect(() => {

      const token = JSON.parse(localStorage.getItem('token'));

      const fetchUser = async () => {
          const result = await Axios.get( process.env.REACT_APP_API_BASE_URL + "/users/user", {
          headers: {
              'authorization': 'Bearer ' + token
          }
          });
          setUser(result.data);
          setOrderLenght(result.data.order)
      };
      
      fetchUser();

    },[]);

    return (
  
      <>
  
    <Header />

      <div id="main-internas">
        
      <Breadcrumb />

        <div className="content internas pedidos">

          <div className="left">

            <FontAwesomeIcon icon={faCheckCircle} />

            <h1>Tudo certo! Seu orçamento foi solicitado.</h1>

            <p>Você receberá o menor orçamento em até 5 dias úteis. Fique tranquilo, assim que estiver tudo pronto você será notificado no email cadastrado.</p>

            <span>Número do pedido 0{total_lenght}</span>

            <Link to="/meus-pedidos">Ir para minha conta</Link>
            
            </div>

            <div className="right">

             <img src={imgPedido} />

            </div>

        </div>

    </div>

        <Footer />
  
      </>
      
    );
  }
  
  export default PedidoConfirmado;