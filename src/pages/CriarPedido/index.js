import React, { useEffect, useState } from "react";
import { useHistory, Link } from "react-router-dom";
import Axios from 'axios';

import { useForm } from "react-hook-form";

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import GoToTop from "../../services/goTop";

import Header from '../../components/Header';
import Footer from '../../components/Footer';
import Breadcrumb from "../../components/Breadcrumb";

import { faHome, faTrashAlt, faAngleRight, faPaperclip } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";



import './criarpedido.css';

const Order = () => {

    const [user, setUser] = useState([]);
    const [address, setUserAddress] = useState([]);

    const [products, setProducts] = useState([]);
    const [category, setProductCategory] = useState('');
    const [quantity, setProductQuantity] = useState('');
    const [name, setProductName] = useState('');
    const [unity, setProductUnity] = useState('');

    const [observation, setObservation] = useState('');
    const [type, setTypeOrder] = useState('');
    const [orderAddress, setOrderAddress] = useState('');

    const handleRemoveItem = idx => {
      const temp = [...products];
      temp.splice(idx, 1);
      setProducts(temp);
    }

    const { handleSubmit } = useForm();
    const history = useHistory();

    function productform() {
    
      const product = {category, name, quantity, unity}

      setProducts(
        products.concat(product),
        document.getElementById('category').value = '',
        document.getElementById('name').value = '',
        document.getElementById('quantity').value = '',
        document.getElementById('unity').value = '',
        setProductCategory(''),
        setProductName(''),
        setProductQuantity(''),
        setProductUnity('')
      );

    };

    function orderForm() {

      const token = JSON.parse(localStorage.getItem('token'));

      let axiosConfig = {
        headers: {
          'authorization': 'Bearer ' + token
        }
      };
    
      const order = new FormData();
      order.append('products', products);
      order.append('observation', observation);
      order.append('type', type);
      order.append('address', orderAddress);

      Axios.post( process.env.REACT_APP_API_BASE_URL + "/orders/new", {
        products: products,
        observation: observation,
        type: type,
        status: 'order',
        value: '--',
        address: orderAddress
      }, axiosConfig)
      .then((res) => {
        history.push("/pedido-confirmado");
      })
      .catch((error) => {
          console.log(error.response.data.error);
          toast.error(error.response.data.error);
      });

    };

    useEffect(() => {

        const token = JSON.parse(localStorage.getItem('token'));

        const fetchUser = async () => {
            const result = await Axios.get( process.env.REACT_APP_API_BASE_URL + "/users/user", {
            headers: {
                'authorization': 'Bearer ' + token
            }
            });
            setUser(result.data);
        };

        const fetchUserAddress = async () => {
          const result = await Axios.get( process.env.REACT_APP_API_BASE_URL + "/address", {
          headers: {
              'authorization': 'Bearer ' + token
          }
          });
          setUserAddress(result.data);
        };
        
        fetchUser();
        fetchUserAddress();

    },[]);

    function Num() {
      var numero = document.getElementById('num');
      numero = 0;
    
      document.getElementById('menos').click(function() {
        numero--;
      });
      document.getElementById('mais').click(function() {
        numero++;
      });
    }

    const prodCategory = [
        { value: 'Geral', label: 'Geral' },
        { value: 'Tintas', label: 'Tintas' }, 
        { value: 'Material Elétrico', label: 'Material Elétrico' }, 
        { value: 'Material Hidráulico', label: 'Material Hidráulico' }, 
        { value: 'Impermeabilização', label: 'Impermeabilização' }, 
        { value: 'Ferramentas', label: 'Ferramentas' }, 
        { value: 'Máquinas e Equipamentos', label: 'Máquinas e Equipamentos' }, 
        { value: 'Aço', label: 'Aço' }, 
        { value: 'Cimento', label: 'Cimento' }, 
        { value: 'Pisos e Revestimentos', label: 'Pisos e Revestimentos' }, 
        { value: 'Mármores e Granitos', label: 'Mármores e Granitos' }, 
        { value: 'Louças e Metais', label: 'Louças e Metais' }, 
        { value: 'Madeiras', label: 'Madeiras' }, 
        { value: 'Ferragens', label: 'Ferragens' }, 
        { value: 'Pré-Moldados', label: 'Pré-Moldados' }, 
        { value: 'Peças de Ferro Fundido', label: 'Peças de Ferro Fundido' }, 
        { value: 'Telhas Cerâmicas', label: 'Telhas Cerâmicas' }, 
        { value: 'Telhas de Fibrocimento', label: 'Telhas de Fibrocimento' }, 
        { value: 'Telhas de Metal', label: 'Telhas de Metal' }, 
        { value: 'EPI', label: 'EPI' }, 
        { value: 'Materiais para Fixação', label: 'Materiais para Fixação' }, 
        { value: 'Dry Wall', label: 'Dry Wall' }, 
        { value: 'Gesso em Placa', label: 'Gesso em Placa' },
        { value: 'Vidros', label: 'Vidros' },
        { value: 'Esquadrias Metálicas', label: 'Esquadrias Metálicas' }
      ]

    return (
  
      <>
  
      <Header />

        <div id="main-internas">
          
        <Breadcrumb />

          <div className="order">

            <div className="left">

              <form className="criar-pedido" onSubmit={handleSubmit(productform)}>

                <div className="lineone">

                    <div>
                      <label>Categoria do produto</label>
                      <select name="category" id="category" onChange={e => setProductCategory(e.target.value)}>
                      {prodCategory.map((item, idx) => (
                        <option value={item.value}>{item.label}</option>
                      ))}
                      </select>
                    </div>

                    <div>
                      <label>Qual produto você procura?</label>
                      <input type="text" placeholder="Ex: Parafuso sextavado 10mm" name="name" id="name" onChange={e => setProductName(e.target.value)} required />
                    </div>

                </div>

                <div className="linetwo">

                    <div>
                      <label>Quantidade</label>
                      <input type="number" name="quantity" id="quantity" onChange={e => setProductQuantity(e.target.value)} required />
                    </div>

                    <div>
                      <label>Unidade</label>
                      <select name="unity" id="unity" onChange={e => setProductUnity(e.target.value)}>
                        <option value="unidades">unidades</option>
                        <option value="mg">mg</option>
                        <option value="g">g</option>
                        <option value="Kg">Kg</option>
                        <option value="m">m</option>
                        <option value="m²">m²</option>
                        <option value="m³">m³</option>
                        <option value="l">L</option>
                        <option value="par">Par</option>
                        <option value="sc">Saco</option>
                        <option value="pç">Peça</option>
                      </select>
                    </div>

                    <button className="btn">Adicionar Produto</button>
                
                </div>

              </form>

              <div className="entrega">

                <h4><span>Endereço de entrega</span></h4>
                
                <select name="address" id="address" onChange={e => setOrderAddress(e.target.value)} >
                    <option>Selecione um endereço pré-cadastrado</option>
                    {address.map((item, idx) => (
                      <option value={item._id}>{item.name}</option>
                    ))}
                </select>

                <p>ou <Link to="/meus-enderecos">clique aqui</Link> para cadastrar um novo endereço de entrega</p>

              </div>
                 
            </div>

            <div className="right">

              <h4 className="title">Orçamento</h4>

              <div className="orcamento">

                  <div className="description">

                        <ul>
                          {products.map((item, idx) => (
                            <li className="row" key={idx}>
                                <span className="product">{idx + 1}. {item.name}</span>
                                <span className="category">{item.category}</span>
                                <span className="units">{item.quantity} {item.unity}</span>
                                <span className="trash"><FontAwesomeIcon icon={faTrashAlt} name={item.name} onClick={() =>handleRemoveItem(idx)} /></span>
                            </li>
                          ))}
                          {products.length > 0 ? (
                            <></>
                          ) : (
                            <li className="row">
                              <span className="product">Nenhum produto adicionado</span>
                            </li>
                          )}
                        </ul>

                </div>

                <div className="note">

                  <h4>Observações</h4>
                  
                  <textarea placeholder="Nos avise qualquer detalhe importante sobre os produtos que prefere." onChange={e => setObservation(e.target.value)}></textarea>

                  {/* <button className="btn"><FontAwesomeIcon icon={faPaperclip} /> Anexar Arquivo</button> */}

                  <div className="clear"></div>
                          
                </div>

              </div>

              <div className="options">

                  <h3>Como você deseja receber seu orçamento?</h3>

                  <label><input type="radio" name="typeorder" value="lessvalue" onChange={e => setTypeOrder(e.target.value)} /> Quero o menor valor total, com fornecedores variados.</label>

                  <label><input type="radio" name="typeorder" value="onesupplier" onChange={e => setTypeOrder(e.target.value)} /> Quero comprar de apenas um fornecedor.</label>

                  <button className="btn green full" onClick={handleSubmit(orderForm)}>Solicitar orçamento</button>

              </div>

            </div>
            
            {/* //fim right */}
      
          </div>

      </div>

      <ToastContainer />

      <Footer />

      <GoToTop />
    
      </>
      
    );
  }
  
  export default Order;