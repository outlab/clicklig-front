import React, { useState } from "react";
import { useHistory, Link } from "react-router-dom";
import { useForm } from "react-hook-form";
import Axios from "axios";

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import GoToTop from "../../services/goTop";

import Header from '../../components/Header';
import Footer from '../../components/Footer';
import Breadcrumb from "../../components/Breadcrumb";

import { faHome} from "@fortawesome/free-solid-svg-icons";
import { faAngleRight} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import FornecedorImg from "../../assets/images/fornecedor.png";

import './style.css';

const Fornecedor = () => {

    const sgMail = require('@sendgrid/mail');
    sgMail.setApiKey(process.env.SENDGRID_API_KEY);

    const [name, setContactName] = useState('');
    const [mail, setContactMail] = useState('');
    const [phone, setContactPhone] = useState('');

    const { handleSubmit } = useForm();

    function contactForm() {

      const contact = new FormData();
      contact.append('name', name);
      contact.append('mail', mail);
      contact.append('phone', phone);
      
      Axios.post( process.env.REACT_APP_API_BASE_URL + "/mail/supplier", {
        name: name,
        mail: mail,
        phone: phone
      })
      .then((res) => {
        document.getElementById("contactform").reset();
        toast.success('Solicitação enviada com sucesso. Em breve retornaremos o seu contato.')
      })
      .catch((error) => {
        console.log(error);
        toast.error('😲' + error)
      });
  
    };

    return (
  
      <>
  
    <Header />

      <div id="main-internas">
        
        <Breadcrumb />

        <div class="content">

        <div class="seja-fornecedor">

        <div class="conteudo-seja-fornecedor">

            <div class="imagem-seja-fornecedor"><img src={FornecedorImg} /></div>

            <div class="form-seja-fornecedor">

              <h3>Seja um Fornecedor</h3>

              <p>Fale com nossos especialistas e cadastre sua loja. Trabalhamos por geolocalização para otimizar suas entregas.</p>

              <form class="form-criar-conta" onSubmit={handleSubmit(contactForm)} id="contactform">

                    <label>Nome</label>
                    <input type="text" placeholder="" onChange={e => setContactName(e.target.value)} required />

                    <label>E-mail</label>
                    <input type="email" placeholder="" onChange={e => setContactMail(e.target.value)} required />

                    <label>Telefone</label>
                    <input type="text" placeholder="" onChange={e => setContactPhone(e.target.value)} required />

                    <button className="btn">Enviar</button>

              </form>

            </div>

        </div>

        </div>

        </div>
   
    </div>

        <ToastContainer />

        <Footer />

        <GoToTop />
  
      </>
      
    );
  }
  
  export default Fornecedor;