import React, { useState } from "react";
import { useHistory, Link } from "react-router-dom";
import { useForm } from "react-hook-form";

import Axios from "axios";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import GoToTop from "../../services/goTop";

import Header from '../../components/Header';
import Footer from '../../components/Footer';
import Breadcrumb from "../../components/Breadcrumb";

import { faHome} from "@fortawesome/free-solid-svg-icons";
import { faAngleRight} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import barraTitle from "../../assets/images/barra-title.svg";

import './style.css';

const ChangePass = () => {

    const [token, setUserToken] = useState('');
    const [email, setUserEmail] = useState('');
    const [password, setUserPass] = useState('');

    const { handleSubmit } = useForm();

    const history = useHistory();

    function forgotPassForm() {
    
      const forgot = new FormData()
      forgot.append('email', email)
      
      Axios.post( process.env.REACT_APP_API_BASE_URL + "/auth/reset_password", {
        email, token, password
      })
      //.then((res) => {
        //toast.success('Um e-mail foi enviado para você. Confira sua caixa de entrada!')
      //})
      .then((res) => {
        history.push("/login");
      })
      .catch((error) => {
          console.log(error.response.data.error);
          toast.error('😲' + error.response.data.error)
      });

    };

    return (
  
      <>

      <ToastContainer />
    
      <Header />

      <div id="main-internas">
        
      <Breadcrumb />

        <div class="content internas">

          <div class="form-login-troque form-login">

              <h3>Troque sua senha</h3>

                <form class="form-login-senha" onSubmit={handleSubmit(forgotPassForm)}>

                <p>Informe o código que você recebeu por e-mail, assim como sua nova senha.</p>

                    <label>Código</label>
                    <input type="text" required onChange={e => setUserToken(e.target.value)} />
                    
                    <label>E-mail</label>
                    <input type="email" required onChange={e => setUserEmail(e.target.value)} />

                    <label>Nova Senha</label>
                    <input type="password" required onChange={e => setUserPass(e.target.value)} />

                    <button class="btn-criar-conta">Trocar a Senha</button>

                    {/* <span><Link to="login">Voltar para o Login</Link></span> */}

                </form>

                </div>

            </div>

    </div>

        <Footer />
        
        <GoToTop />

      </>
      
    );
  }
  
  export default ChangePass;