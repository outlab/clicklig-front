import React, { useState } from "react";
import { useHistory, Link } from "react-router-dom";

import GoToTop from "../../services/goTop";

import Header from '../../components/Header';
import Footer from '../../components/Footer';
import Breadcrumb from "../../components/Breadcrumb";
import CreateAccountForm from '../../components/CreateAccountForm';

import { faHome} from "@fortawesome/free-solid-svg-icons";
import { faAngleRight} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import barraTitle from "../../assets/images/barra-title.svg";

import './conta.css';

const CriarConta = () => {

    return (
  
      <>
  
    <Header />

      <div id="main-internas">
        
      <Breadcrumb />

        <div className="content internas criar-conta">

          <div class="left">

            <div class="title"><h1>Garantia de menor preço</h1></div>

            <div class="barra-title"><img class="banner" src={barraTitle} /></div>

            <p class="text-conta">
              Comparamos entre os melhores fornecedores para garantir que você terá o menor preço sem perder a qualidade e sem sair de casa. Conheça o ClickLig.
              </p>
            
          </div>

          <div class="right">

            <CreateAccountForm />
          
          </div>

        </div>

    </div>

        <Footer />

        <GoToTop />
  
      </>
      
    );
  }
  
  export default CriarConta;