import React, { useState } from "react";
import { useHistory, Link } from "react-router-dom";

import ReactModal from 'react-modal';

import GoToTop from "../../services/goTop";

import Header from '../../components/Header';
import Footer from '../../components/Footer';
import Beneficios from '../../components/Beneficios';
import CreateAccount from '../../components/CreateAccount';
import Partners from '../../components/Partners';

import bannerHome from "../../assets/images/banner.png";
import bannerMobile from "../../assets/images/bannerMobile.png";
import iconBoxUm from "../../assets/images/icon1.png";
import iconBoxDois from "../../assets/images/icon2.png";
import iconBoxTres from "../../assets/images/icon3.png";
import iconBoxQuatro from "../../assets/images/icon4.png";
import materiaisConstrucao from "../../assets/images/mais-detalhes.png";

import { faPlayCircle, faTimes } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import './home.css';

const Home = () => {

  const [showModal, setshowModal] = useState(false);

  return (

    <>

      <div className="video">
      <ReactModal 
          isOpen={showModal}
          contentLabel="Minimal Modal Example"
          className="Modal"
      >

        <FontAwesomeIcon icon={faTimes} onClick={e => setshowModal(false)} />
      
        <video controls controlsList="nodownload">
          <source src="https://clicklig.com.br/files/conheca-clicklig.mp4" type="video/mp4" />
        </video>

      </ReactModal>

      <div className="overlay"></div>
      </div>

      <Header />

      <div class="slider-home">
      
        <div class="conteudo-slider">

          <div class="left">
            
              <h1>Procurando materiais de construção em um só lugar?</h1>

              <p>Achou! Obra com praticidade e economia é com o ClickLig</p>

              <ul class="btns-slider">
                  <li class="btn-orcar"><Link to="fazer-pedido" className="btn">Quero orçar agora</Link></li>
                  <li onClick={e => setshowModal(true)} className="modalopen"><i class="fas"><FontAwesomeIcon icon={faPlayCircle} /></i> Ver o ClickLig em ação</li>
              </ul>  

          </div>

          <div class="right">

             <img class="banner" src={bannerHome} />
             <img class="banner-mobile" src={bannerMobile} />

          </div>
       

        </div>

      </div>

      <div class="carousel-home">

            <h2>Conheça os benefícios de usar o <strong>ClickLig</strong></h2>

            <Beneficios />

            <div className="clear"></div>

        </div>

        <div class="materiais">

          <div class="textos">

              <h2>Materiais de construção de maneira fácil e econômica</h2>

              <p>As melhores marcas e modelos em tintas, material elétrico, hidráulico, impermeabilização,
                 ferramentas, aço, cimento, pisos, revestimentos, mármores, granitos, gesso e muito mais.</p>

           </div>

           <img src={materiaisConstrucao} alt="" />

           <div className="clear"></div>

        </div>

      <div class="content">

        <div class="como-funciona">

          <p>Tudo para sua obra</p>

          <h2>Como funciona?</h2>

          <div class="block-functions">

            <div class="box-function">

                <div class="icon">

                  <img src={iconBoxUm} />
                  
                </div>
                
                <h4>Crie Orçamentos</h4>

                <p>Insira sua lista de materiais com os itens necessários para a sua obra.</p>

            </div>

            <div class="box-function">

                <div class="icon">

                  <img src={iconBoxDois} />
                  
                </div>

                <h4>Receba os melhores</h4>

                <p>Você escolhe entre receber o menor valor de um único fornecedor, ou o menor valor total de vários fornecedores.</p>

            </div>

            <div class="box-function">

                <div class="icon">

                  <img src={iconBoxTres} />
                  
                </div>

                <h4>Faça seu pedido</h4>

                <p>Finalize sua compra realizando o pagamento por dentro do próprio sistema.</p>

            </div>

            <div class="box-function">

                <div class="icon">

                  <img src={iconBoxQuatro} />
                  
                </div>

                <h4>Receba nos seus endereços</h4>

                <p>Seus materiais são entregues diretamente nos endereços informados de sua obra.</p>

              </div>

          </div>

          <span class="btn-orcar">
            <Link to="/fazer-pedido" className="btn">Quero orçar agora</Link>
          </span>

        </div>

        {/* <div class="parceiros">

            <h2>Nossos Parceiros</h2>

            <Partners />

        </div> */}

                <CreateAccount />

      </div>

      <Footer />

      <GoToTop />

    </>
    
  );
}

export default Home;