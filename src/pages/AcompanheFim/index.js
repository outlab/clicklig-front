import React, { useState } from "react";
import { useHistory, Link } from "react-router-dom";

import Header from '../../components/Header';
import Footer from '../../components/Footer';

import { faHome} from "@fortawesome/free-solid-svg-icons";
import { faAngleRight} from "@fortawesome/free-solid-svg-icons";
import { faShoppingCart} from "@fortawesome/free-solid-svg-icons";
import { faMapMarkerAlt} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import barraTitle from "../../assets/images/barra-title.svg";
import Linha from "../../assets/images/etapaquatro.png";
import LinhaEtapa from "../../assets/images/linhaetapa.png";


import './acompanhefim.css';

const AcompanheFim = () => {

    return (
  
      <>
  
        <Header />

      <div id="main-internas">
        
        <div class="path">
          
        <ul>
            <li><a href="#"><i class="fas"><FontAwesomeIcon icon={faHome} /></i> Home</a></li>
            <li><i class="fas"><FontAwesomeIcon icon={faAngleRight} /></i></li>
            <li><a href="#">Acompanhe seu pedido</a></li>
          </ul>

        </div>

        <div class="content internas etapas">

        <div class="title"><h1>Acompanhe seu pedido</h1></div>

        <div class="barra-title"><img class="banner" src={barraTitle} /></div>

        <div class="etapas-linha">

            <div class="linha"><img class="banner" src={Linha} /></div>

            <ul>
                <li class="concluido">Pedido Enviado</li>
                <li class="concluido">Orçamento Recebido</li>
                <li class="concluido">Compra solicitada</li>
                <li class="concluido">Compra finalizada</li>
            </ul>

            <div class="boxe-pedido-quatro">
                <h4>Obra 1</h4>
                <span>Confira abaixo o orçamento com melhor custo-benefício para você.</span>
            </div>
            
        </div>

        <div class="details-pedido">

            <div class="left">

                    <div class="details">

                        <div class="box">

                        <h4 class="title"><i class="fas"><FontAwesomeIcon icon={faShoppingCart} /></i> Detalhes do pedido</h4>

                        <ul>
                            <li>Obra 1</li>
                            <li>nº 0001</li>
                            <li>28/07/2021</li>
                        </ul>

                        </div>

                    </div>

                <div class="address">

                 <div class="details">

                    <div class="box">

                    <h4 class="title"><i class="fas"><FontAwesomeIcon icon={faMapMarkerAlt} /></i> Endereço</h4>

                    <ul>
                        <li>Obra 1</li>
                        <li>
                            <span>
                                <p>Carlos Ferreira </p>
                                <p>Rua das Acácias, 65 - apto 202</p>
                                <p>CEP 22345-090 - Centro</p>
                                <p>Rio de Janeiro/ RJ</p>
                            </span>
                        </li>
                    </ul>

                    </div>

                    </div>
                </div>

            </div>

            <div class="right">

                <div class="description">

                        <ul>
                            <li class="row">
                                <span class="product">Parafusos sextavado 10mm</span>
                                <span class="units">80 unidades</span>
                            </li>
                            <li class="row">
                                <span class="product">Bloco ceramico vermelho de vedação</span>
                                <span class="units">500 unidades</span>
                            </li>
                            <li class="row">
                                <span class="product">Bloco ceramico vermelho de vedação</span>
                                <span class="units">500 unidades</span>
                            </li>
                            <li class="row">
                                <span class="product">Bloco ceramico vermelho de vedação</span>
                                <span class="units">500 unidades</span>
                            </li>
                        </ul>

                        <img class="separador" src={LinhaEtapa} />

                        <ul class="value">
                            <li class="valor">                                
                                    <span>Menor valor com um fornecedor</span><br></br>
                                    <a href="#">Abrir PDF com detalhes</a>
                            </li>
                            <li class="valor-number">R$ 678,90</li>
                        </ul>

                </div>

            </div>


        </div>
      
      </div>

     </div>

        <Footer />
  
      </>
      
    );
  }
  
  export default AcompanheFim;