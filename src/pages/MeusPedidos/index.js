import React, { useEffect, useState } from "react";
import { useHistory, Link } from "react-router-dom";
import Axios from 'axios';

import GoToTop from "../../services/goTop";

import Header from '../../components/Header';
import Footer from '../../components/Footer';

import { faHome, faAngleRight, faUser, faMapMarkerAlt, faBoxOpen, faEye } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import barraTitle from "../../assets/images/barra-title.svg";
import Linha from "../../assets/images/etapaum.png";

import UserSideMenu from "../../components/UserSideMenu";

import './style.css';

const MyOrders = () => {

    const [orders, setOrders] = useState([]);
    const [user, setUser] = useState([]);

    useEffect(() => {

        const token = JSON.parse(localStorage.getItem('token'));

        const fetchUser = async () => {
            const result = await Axios.get( process.env.REACT_APP_API_BASE_URL + "/users/user", {
            headers: {
                'authorization': 'Bearer ' + token
            }
            });
            setUser(result.data);
        };
        
        fetchUser();

        const fetchOrders = async () => {
            const result = await Axios.get( process.env.REACT_APP_API_BASE_URL + "/orders", {
            headers: {
                'authorization': 'Bearer ' + token
            }
            });
            setOrders(result.data);
        };
        
        fetchOrders();

    },[]);

    return (
  
      <>
  
    <Header />

      <div id="main-internas">
        
        <div className="path">
          
        <ul>
            <li><a href="#"><i className="fas"><FontAwesomeIcon icon={faHome} /></i> Home</a></li>
            <li><i className="fas"><FontAwesomeIcon icon={faAngleRight} /></i></li>
            <li><a href="#">Meus Dados</a></li>
          </ul>

        </div>

        <div className="content">

                <div className="meus-dados">

                    <UserSideMenu />

                    <div className="right">

                        <h4 className="linetitle"><span>Histórico de Pedidos</span></h4>

                        <section className="orderlist">

                                <div className="orderrow headerrow">
                                    <div className="date">Data</div>
                                    <div className="address">Obra</div>
                                    <div className="ordernumber">Nº Pedido</div>
                                    <div className="value">Valor</div>
                                    <div className="status">Status</div>
                                    <div className="view"></div>
                                </div>

                            { orders.length > 0 
                                ? 
                                orders.map((item, idx) => (
                                    <div className="orderrow" key={idx}>
                                        <div className="date">{item.createdAt.toString().slice(0, 10).split('-').reverse().join('/')}</div>
                                        <div className="address">{item.address.name}</div>
                                        <div className="ordernumber">0{idx + 1}</div>
                                        <div className="value">{item.value}</div>
                                        <div className="status">
                                            <button className={`${item.status} btn`}></button>
                                        </div>
                                        <div className="view">
                                            <Link to={`/acompanhe-seu-pedido/${item._id}`}><FontAwesomeIcon icon={faEye} /></Link>
                                        </div>
                                    </div>
                                ))
                                : <p className="noorder">Você ainda não realizou nenhum pedido.</p>
                            }

                            {/* {orders.map((item, idx) => (
                                <div className="orderrow" key={idx}>
                                    <div className="date">{item.createdAt.toString().slice(0, 10).split('-').reverse().join('/')}</div>
                                    <div className="address">{item.address.name}</div>
                                    <div className="ordernumber">0{idx + 1}</div>
                                    <div className="value">{item.value}</div>
                                    <div className="status">
                                        <button className={`${item.status} btn`}></button>
                                    </div>
                                    <div className="view">
                                        <Link to={`/acompanhe-seu-pedido/${item._id}`}><FontAwesomeIcon icon={faEye} /></Link>
                                    </div>
                                </div>
                            )) : <p>Nenhum</p> } */}

                        </section>

                    </div>

                </div>

     
        </div>

     </div>

    <Footer />

    <GoToTop />
  
      </>
      
    );
  }
  
  export default MyOrders;