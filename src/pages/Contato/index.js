import React, { useState } from "react";
import { useHistory, Link } from "react-router-dom";
import { useForm } from "react-hook-form";
import Axios from "axios";

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import GoToTop from "../../services/goTop";

import Header from '../../components/Header';
import Footer from '../../components/Footer';
import Breadcrumb from "../../components/Breadcrumb";

import { faHome} from "@fortawesome/free-solid-svg-icons";
import { faAngleRight} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import ContatoImg from "../../assets/images/contato.png";

import './contato.css';

const Contato = () => {

    const sgMail = require('@sendgrid/mail');
    sgMail.setApiKey(process.env.SENDGRID_API_KEY);

    const [name, setContactName] = useState('');
    const [mail, setContactMail] = useState('');
    const [message, setContactMessage] = useState('');

    const { handleSubmit } = useForm();

    function contactForm() {

      const contact = new FormData();
      contact.append('name', name);
      contact.append('mail', mail);
      contact.append('message', message);
      
      Axios.post( process.env.REACT_APP_API_BASE_URL + "/mail/contact", {
        name: name,
        mail: mail,
        message: message
      })
      .then((res) => {
        document.getElementById("contactform").reset();
        toast.success('Mensagem enviada com sucesso. Em breve retornaremos o seu contato.')
      })
      .catch((error) => {
        console.log(error);
        toast.error('😲' + error)
      });
  
    };

    return (
  
      <>
  
    <Header />

      <div id="main-internas">
        
        <Breadcrumb />

        <div class="content">

        <div class="fale-conosco">

        <div class="conteudo-fale-conosco">

            <div class="imagem-fale-conosco"><img src={ContatoImg} /></div>

            <div class="form-fale-conosco">

              <h3>Fale Conosco</h3>

              <p>Responderemos o mais prontamente possível</p>

              <form class="form-criar-conta" onSubmit={handleSubmit(contactForm)} id="contactform">

                    <label>Nome</label>
                    <input type="text" placeholder="" onChange={e => setContactName(e.target.value)} required />

                    <label>E-mail</label>
                    <input type="text" placeholder="" onChange={e => setContactMail(e.target.value)} required />

                    <label>Mensagem</label>
                    <textarea onChange={e => setContactMessage(e.target.value)}></textarea>

                    <button className="btn">Enviar</button>

              </form>

            </div>

        </div>

        </div>

        </div>
   
    </div>

        <ToastContainer />

        <Footer />

        <GoToTop />
  
      </>
      
    );
  }
  
  export default Contato;