import React, { useState } from "react";
import { useHistory, Link } from "react-router-dom";

import GoToTop from "../../services/goTop";

import Header from '../../components/Header';
import Footer from '../../components/Footer';
import Breadcrumb from "../../components/Breadcrumb";

import { faHome} from "@fortawesome/free-solid-svg-icons";
import { faAngleRight} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import barraTitle from "../../assets/images/barra-title.svg";

const Terms = () => {

    return (
  
      <>
  
        <Header />

      <div id="main-internas">
        
      <Breadcrumb />

        <div class="content internas">

            <div class="title"><h1>Termos e Condições</h1></div>

            <div class="barra-title"><img class="banner" src={barraTitle} /></div>

            <p><strong>Id mattis dui id cursus. Pretium pellentesque sed proin morbi sed commodo nunc pulvinar. At fusce praesent ac enim lectus tincidunt donec nisl, pharetra. Tellus sit in cursus a blandit posuere.</strong></p>

            <p>Tellus enim, mattis pretium, sit ornare massa. Sed hendrerit venenatis amet turpis scelerisque eleifend amet elit. Suscipit egestas auctor justo nunc. Neque, eleifend morbi aliquet ut tincidunt. Vitae vivamus in morbi amet urna risus. Mi, ullamcorper eu purus eget. Sit in sodales tristique nec nunc fames sed volutpat ultrices. Lectus hac aenean nisl nulla senectus sed elit lectus id. Mollis semper euismod auctor senectus ut.
                Turpis sit ultrices ante euismod eu vitae ultrices amet. Euismod at duis senectus in sed nullam at felis libero. Integer at nunc lectus quis semper. Feugiat non, tristique nisl vitae morbi elementum. Amet mauris aenean amet egestas vulputate. Eleifend quis velit pretium cursus non. Erat ut vitae libero sit eget donec odio commodo, tellus. Justo faucibus et mi tincidunt augue fermentum. Dui aliquet lacinia elit lacus lacus sit diam egestas aenean. Amet aenean egestas in tortor nulla pellentesque vestibulum magna. Vulputate arcu tortor in at at egestas.</p>

                <p>Adipiscing lacus iaculis massa tellus dui maecenas rhoncus gravida interdum. Augue fames tortor, ipsum venenatis elit molestie amet fermentum. Quam morbi molestie interdum tristique nibh. Aliquet nulla elit et dui sem sagittis tortor tellus commodo. Quisque luctus ac eu et euismod sed massa. Morbi iaculis suscipit aliquet erat feugiat ipsum malesuada mi nec. Non id commodo mauris aliquet amet dignissim congue. Donec consectetur nisl blandit aliquam, augue ultricies eu aliquam, egestas. Convallis tortor, sed vitae malesuada tempor eleifend ultrices.
                Justo vitae et nibh eu netus bibendum quisque nunc. In et, lectus sit risus amet eget. Ultricies sapien phasellus nibh sed netus. Arcu sit odio lorem lacinia id felis. Vel quis quis faucibus diam urna mauris vitae. Blandit rutrum elementum nibh egestas magna molestie. Vulputate eu, arcu, a in diam. Eget accumsan nullam pulvinar et quam.</p>

                <p> Nunc adipiscing egestas adipiscing condimentum cursus orci, vitae adipiscing. Diam dictumst amet, praesent facilisis sollicitudin montes, a. Elementum viverra tempor, neque amet. At elit lorem imperdiet elementum gravida et sit scelerisque. Tellus duis integer tellus netus risus. Magna est mauris morbi vivamus porttitor molestie.</p>

                <p>Gravida dapibus mauris nunc quam lobortis vestibulum elit. Est sed tempus eleifend feugiat adipiscing nullam. Porta volutpat eget a, dolor, facilisis ultricies vulputate pellentesque aliquet. Ultricies varius dapibus porttitor diam convallis. Gravida leo non risus suspendisse. Eros, nulla in nunc ultrices. Nisi ultrices placerat eget blandit sit purus est molestie. Nisi aliquet arcu arcu, semper lectus. Egestas tellus id facilisis pellentesque orci, tincidunt fusce. Facilisis venenatis ornare eros, condimentum iaculis ornare amet at. Maecenas semper tincidunt amet, elit amet tempus purus, vitae. Dignissim commodo habitasse magna gravida elementum. Scelerisque aliquam arcu risus aliquet posuere malesuada egestas molestie.</p>

                <p>Egestas ipsum quis in vitae, suspendisse sed elementum viverra. Ultricies arcu viverra mauris sed posuere nisi. Eu velit pellentesque rutrum ornare consectetur mattis. Orci, mauris sed tortor augue in arcu pellentesque. Feugiat vitae, malesuada eleifend sed nisi. Lectus diam vitae venenatis rhoncus proin. Commodo nisi amet id laoreet mi duis. Ac hendrerit in id lobortis sollicitudin vestibulum pretium aliquam sapien.</p>

                <p>Nulla pellentesque pellentesque proin sed ultrices neque, dolor, urna arcu. Adipiscing dolor, cras etiam in erat bibendum feugiat. Accumsan proin quam in quam hendrerit commodo. Viverra gravida pellentesque porttitor nisi, congue. Elit faucibus leo, nunc, felis, vitae nisi praesent. Tortor tellus congue maecenas duis cursus. Enim, non in pellentesque nisi, urna erat pellentesque tincidunt neque. Phasellus volutpat fusce in massa. Ut sed egestas aliquet ut ac commodo dictum integer neque. Varius tristique molestie sed ullamcorper elit. Egestas nec egestas proin sem fusce scelerisque arcu amet sed. Fringilla tortor diam donec ipsum cras sit nisi mattis risus. Nibh morbi amet bibendum tellus. Risus, sit lacus vehicula molestie. Ut rhoncus duis volutpat viverra vel proin a quis vitae.</p>

        </div>

        </div>

 
        <Footer />

        <GoToTop />
  
      </>
      
    );
  }
  
  export default Terms;