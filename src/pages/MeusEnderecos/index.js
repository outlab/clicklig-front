import React, { useEffect, useState } from "react";
import { useHistory, Link } from "react-router-dom";
import Axios from 'axios';

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import Header from '../../components/Header';
import Footer from '../../components/Footer';
import Breadcrumb from "../../components/Breadcrumb";

import { faTrashAlt, faPencilAlt } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import barraTitle from "../../assets/images/barra-title.svg";
import Linha from "../../assets/images/etapaum.png";

import CreateAddressForm from "../../components/CreateAddressForm";

import UserSideMenu from "../../components/UserSideMenu";

import './enderecos.css';

const Enderecos = () => {

    const selectStates = document.querySelector("#estado");
    const selectCities = document.querySelector("#cidades");

    const [address, setAddress] = useState([]);

    const token = JSON.parse(localStorage.getItem('token'));

    const deleteAddress = (event) =>{

        Axios.delete( process.env.REACT_APP_API_BASE_URL + "/address/delete/" + event, {
          headers: {
            'authorization': 'Bearer ' + token
          }
        })
        .then((res) => {
            toast.success('O endereço foi excluído com sucesso.')
            window.location.reload();
        })
        .catch((error) => {
            console.log(error.response.data.error);
            toast.error('😲' + error.response.data.error)
        });
        
      }

    useEffect(() => {

        const fetchAddress = async () => {
            const result = await Axios.get( process.env.REACT_APP_API_BASE_URL + "/address", {
            headers: {
                'authorization': 'Bearer ' + token
            }
            });
            setAddress(result.data);
        };
        
        fetchAddress();

    },[]);

    return (
  
      <>
  
    <Header />

      <div id="main-internas">
        
      <Breadcrumb />

        <div class="content">

                <div class="meus-dados">

                    <UserSideMenu />

                    <div class="right">

                        <CreateAddressForm />

                        <h4 className="linetitle"><span>Endereços</span></h4>

                        <div class="boxes-end">

                            {address.map((item, idx) => (
                            
                                <div class="boxe-white" key={idx}>

                                    {/* <FontAwesomeIcon icon={faPencilAlt} className="pencilicon" /> */}

                                    <FontAwesomeIcon icon={faTrashAlt} className="trashicon" onClick={(e) => {if(window.confirm('Deseja deletar esse endereço?')){deleteAddress(item._id)};}} />

                                    <h4>{item.name}</h4>
                                    <span>  
                                        <p>{item.street}, {item.number}</p>
                                        <p>{item.complement}</p>
                                        <p>CEP: {item.cep}</p>
                                        <p>Bairro: {item.district}</p>
                                        <p>Cidade: {item.city}</p>
                                        <p>Estado: {item.state}</p>
                                    </span>

                                </div>

                            ))}

                        </div>

                    </div>

                </div>

     
        </div>

     </div>

    <Footer />

    <ToastContainer />
  
      </>
      
    );
  }
  
  export default Enderecos;