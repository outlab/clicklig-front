import React, { useState } from "react";
import { useHistory, Link } from "react-router-dom";
import { useForm } from "react-hook-form";

import Axios from "axios";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import GoToTop from "../../services/goTop";

import Header from '../../components/Header';
import Footer from '../../components/Footer';
import Breadcrumb from "../../components/Breadcrumb";

import { faHome} from "@fortawesome/free-solid-svg-icons";
import { faAngleRight} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import barraTitle from "../../assets/images/barra-title.svg";

import './troque.css';

const TroqueSenha = () => {

    const [email, setUserEmail] = useState('');

    const { handleSubmit } = useForm();

    const history = useHistory();

    function forgotPassForm() {
    
      const forgot = new FormData()
      forgot.append('email', email)
      
      Axios.post( process.env.REACT_APP_API_BASE_URL + "/auth/forgot_password", {
        email
      })
      .then((res) => {
        toast.success('Um e-mail foi enviado para você. Confira sua caixa de entrada!')
      })
      .then((res) => {
        history.push("/trocar-senha");
      })
      .catch((error) => {
          console.log(error.response.data.error);
          toast.error('😲' + error.response.data.error)
      });

    };

    return (
  
      <>

      <ToastContainer />
    
      <Header />

      <div id="main-internas">
        
      <Breadcrumb />

        <div class="content internas">

          <div class="form-login-troque form-login">

              <h3>Esqueceu sua senha?</h3>

              <p>Para alterar sua senha insira seu email para receber o código de troca</p>

                <form class="form-login-senha" onSubmit={handleSubmit(forgotPassForm)}>

                      <label>E-mail</label>
                      <input type="email" required onChange={e => setUserEmail(e.target.value)} />

                      <button class="btn-criar-conta">Solicitar código</button>

                      <span><Link to="login">Voltar para o Login</Link></span>

                </form>

                </div>

            </div>

    </div>

        <Footer />
        
        <GoToTop />

      </>
      
    );
  }
  
  export default TroqueSenha;