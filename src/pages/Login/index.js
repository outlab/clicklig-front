import React, { useState } from "react";
import { useHistory, Link } from "react-router-dom";

import { useForm } from "react-hook-form";

import Axios from "axios";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import InputMask from 'react-input-mask';

import GoToTop from "../../services/goTop";

import Header from '../../components/Header';
import Footer from '../../components/Footer';
import Breadcrumb from "../../components/Breadcrumb";

import { faHome} from "@fortawesome/free-solid-svg-icons";
import { faAngleRight} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import barraTitle from "../../assets/images/barra-title.svg";

import './login.css';

  const Login = () => {

    const [email, setUserEmail] = useState('');
    const [password, setUserPassword] = useState('');

    const { handleSubmit } = useForm();

    const history = useHistory();

    function loginForm() {
    
      const login = new FormData()
      login.append('email', email)
      login.append('password', password)
      
      Axios.post( process.env.REACT_APP_API_BASE_URL + "/auth/authenticate", {
        email: email,
        password: password
      })
      .then((res) => {
        localStorage.setItem('token', JSON.stringify(res.data.token));
      })
      .then((res) => {
        history.push("/meus-dados");
      })
      .catch((error) => {
          console.log(error.response.data.error);
          toast.error('😲' + error.response.data.error)
      });

    };

    return (
  
      <>

      <ToastContainer />
  
    <Header />

      <div id="main-internas">
        
        <Breadcrumb />

        <div class="content internas">

          <div class="form-login">

              <h3>Entrar na conta</h3>

              <p>Ainda não tem uma conta? <span><Link to="criar-conta">Inscreva-se agora</Link></span></p>

                <form class="form-login-senha" onSubmit={handleSubmit(loginForm)} id="loginform">

                      <label htmlFor="email">E-mail</label>
                      <input type="email" id="email" name="email" onChange={e => setUserEmail(e.target.value)} />

                      <label>Senha</label>
                      <input type="password" id="password" name="password" onChange={e => setUserPassword(e.target.value)} />

                      <button class="btn">Entrar</button>

                      <span><Link to="esqueci-a-senha">Esqueceu sua senha?</Link></span>

                </form>

                </div>

            </div>

    </div>

        <Footer />

        <GoToTop />
  
      </>
      
    );
  }
  
  export default Login;