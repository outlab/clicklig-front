import React, { useEffect, useState } from "react";
import { useHistory, Link } from "react-router-dom";
import Axios from 'axios';

import { useForm } from "react-hook-form";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import GoToTop from "../../services/goTop";

import Header from '../../components/Header';
import Footer from '../../components/Footer';
import Breadcrumb from "../../components/Breadcrumb";

import UserSideMenu from "../../components/UserSideMenu";

import './style.css';

const MeusDados = () => {

    const [user, setUser] = useState([]);

    const [user_update, setUserData] = useState({});

    const { handleSubmit } = useForm();
    const history = useHistory();

    function editUserForm() {
        
        const token = JSON.parse(localStorage.getItem('token'));

        let axiosConfig = {
            headers: {
                'authorization': 'Bearer ' + token
            }
        };

        Axios.put( process.env.REACT_APP_API_BASE_URL + "/auth/edit_user/" + user._id, {
            user_update
        }, axiosConfig)
        .then((res) => {
            toast.success('Seus dados foram atualizados com sucesso!')
            window.location.reload();
        })
        .catch((error) => {
            console.log(error.response.data.error);
            toast.error('😲' + error.response.data.error)
        });

    };

    useEffect(() => {

        const token = JSON.parse(localStorage.getItem('token'));

        const fetchUser = async () => {
            const result = await Axios.get( process.env.REACT_APP_API_BASE_URL + "/users/user", {
            headers: {
                'authorization': 'Bearer ' + token
            }
            });
            setUser(result.data);
        };
        
        fetchUser();

    },[]);

    return (
  
      <>
  
    <Header />

      <div id="main-internas">
        
      <Breadcrumb />

        <div className="content">

                <div className="meus-dados">

                    <UserSideMenu />

                    <div className="right">
                        
                        <h4 className="linetitle"><span>Dados do Comprador</span></h4>

                        <form onSubmit={handleSubmit(editUserForm)} id="userform">

                            <div className="dados-interna">

                                <div className="dado-one">

                                    <label htmlFor="name">Nome</label>
                                    {user.name ? (
                                        <input className="active" type="text" name="name" id="name" defaultValue={user.name} onChange={e => setUserData({ ...user_update, name: e.target.value} )} />
                                    ) : 
                                        <input type="text" name="name" id="name" defaultValue="" onChange={e => setUserData({ ...user_update, name: e.target.value} )} />
                                    }

                                </div>

                                <div className="dado-two">

                                    <label htmlFor="email">E-mail<span>*</span></label>
                                    {user.email ? (
                                        <input className="active" type="email" name="email" id="email" defaultValue={user.email} onChange={e => setUserData({ ...user_update, email: e.target.value} )} />
                                    ) : 
                                        <input type="email" name="email" id="email" defaultValue="" onChange={e => setUserData({ ...user_update, email: e.target.value} )} />
                                    }

                                </div>

                            </div>
                            <div className="dados-interna">

                                <div className="dado-one">

                                    <label htmlFor="phone">Telefone</label>
                                    {user.phone ? (
                                        <input className="active" type="text" name="phone" id="phone" defaultValue={user.phone} onChange={e => setUserData({ ...user_update, phone: e.target.value} )} />
                                    ) : 
                                        <input type="text" name="phone" id="phone" defaultValue="" onChange={e => setUserData({ ...user_update, phone: e.target.value} )} />
                                    }

                                </div>

                                <div className="dado-two">

                                    <label htmlFor="cpf">CPF ou CNPJ<span>*</span></label>

                                    {user.cpf ? (
                                        <input className="active" type="text" name="cpf" id="cpf" defaultValue={user.cpf} onChange={e => setUserData({ ...user_update, cpf: e.target.value} )} />
                                    ) : 
                                        <input type="text" name="cpf" id="cpf" defaultValue="" onChange={e => setUserData({ ...user_update, cpf: e.target.value} )} />
                                    }
                                
                                </div>

                            </div>

                            <div className="dados-interna">

                                <div className="dado-one">

                                    <label htmlFor="rg">RG</label>
                                    {user.rg ? (
                                        <input className="active" type="text" name="rg" id="rg" defaultValue={user.rg} onChange={e => setUserData({ ...user_update, rg: e.target.value} )} />
                                    ) : 
                                        <input type="text" name="rg" id="rg" defaultValue="" onChange={e => setUserData({ ...user_update, rg: e.target.value} )} />
                                    }

                                </div>

                                <div className="dado-two">

                                    <label htmlFor="born_date">Data de Nascimento</label>

                                    {user.born_date ? (
                                        <input className="active" type="text" name="born_date" id="born_date" defaultValue={user.born_date} onChange={e => setUserData({ ...user_update, born_date: e.target.value} )} />
                                    ) : 
                                        <input type="text" name="born_date" id="born_date" defaultValue="" onChange={e => setUserData({ ...user_update, born_date: e.target.value} )} />
                                    }
                                
                                </div>

                            </div>
                          
                            <button className="btn green">Salvar Alterações</button>

                        </form>

                    </div>

                </div>

     
        </div>

     </div>

    <Footer />

    <GoToTop />

    <ToastContainer />
  
      </>
      
    );
  }
  
  export default MeusDados;