import React, { useState } from "react";
import { useHistory, Link } from "react-router-dom";

import GoToTop from '../../services/goTop';

import Header from '../../components/Header';
import Footer from '../../components/Footer';
import Breadcrumb from "../../components/Breadcrumb";

import { faHome} from "@fortawesome/free-solid-svg-icons";
import { faAngleRight} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import barraTitle from "../../assets/images/barra-title.svg";

import './faq.css';

const Faq = () => {

    return (
  
      <>
  
        <Header />

      <div id="main-internas">
        
      <Breadcrumb />

        <div class="content internas">

        <div class="title"><h1>Faq | Central de Ajuda</h1></div>

        <div class="barra-title"><img class="banner" src={barraTitle} /></div>

        <div class="faq">

            <div class="tabs">

              <div class="tab">
                <input type="checkbox" id="chck1" />
                <label class="tab-label" for="chck1">Id mattis dui id cursus. Pretium pellentesque sed proin morbi sed commodo nunc pulvinar. At fusce praesent ac enim lectus tincidunt donec nisl, pharetra. Tellus sit in cursus a blandit posuere.</label>
                <div class="tab-content">
                Risus etiam ipsum eu, posuere lectus viverra. Egestas massa quis penatibus sem et duis dolor. Praesent aliquet et justo vel volutpat, volutpat euismod ut lorem. Massa quis et ullamcorper etiam sit sed morbi mi. Tortor vehicula cursus arcu nibh et sed suscipit ipsum. Imperdiet congue dis mi pretium. Interdum egestas varius quis fames tortor.
                </div>
              </div>

              <div class="tab">
                <input type="checkbox" id="chck2" />
                <label class="tab-label" for="chck2">Id mattis dui id cursus. Pretium pellentesque sed proin morbi sed commodo nunc pulvinar. At fusce praesent ac enim lectus tincidunt donec nisl, pharetra. Tellus sit in cursus a blandit posuere.</label>
                <div class="tab-content">
                Risus etiam ipsum eu, posuere lectus viverra. Egestas massa quis penatibus sem et duis dolor. Praesent aliquet et justo vel volutpat, volutpat euismod ut lorem. Massa quis et ullamcorper etiam sit sed morbi mi. Tortor vehicula cursus arcu nibh et sed suscipit ipsum. Imperdiet congue dis mi pretium. Interdum egestas varius quis fames tortor.
                </div>              
              </div>

              <div class="tab">
                <input type="checkbox" id="chck3" />
                <label class="tab-label" for="chck3">Id mattis dui id cursus. Pretium pellentesque sed proin morbi sed commodo nunc pulvinar. At fusce praesent ac enim lectus tincidunt donec nisl, pharetra. Tellus sit in cursus a blandit posuere.</label>
                <div class="tab-content">
                Risus etiam ipsum eu, posuere lectus viverra. Egestas massa quis penatibus sem et duis dolor. Praesent aliquet et justo vel volutpat, volutpat euismod ut lorem. Massa quis et ullamcorper etiam sit sed morbi mi. Tortor vehicula cursus arcu nibh et sed suscipit ipsum. Imperdiet congue dis mi pretium. Interdum egestas varius quis fames tortor.
                </div>              
              </div>

              <div class="tab">
                <input type="checkbox" id="chck4" />
                <label class="tab-label" for="chck4">Id mattis dui id cursus. Pretium pellentesque sed proin morbi sed commodo nunc pulvinar. At fusce praesent ac enim lectus tincidunt donec nisl, pharetra. Tellus sit in cursus a blandit posuere.</label>
                <div class="tab-content">
                Risus etiam ipsum eu, posuere lectus viverra. Egestas massa quis penatibus sem et duis dolor. Praesent aliquet et justo vel volutpat, volutpat euismod ut lorem. Massa quis et ullamcorper etiam sit sed morbi mi. Tortor vehicula cursus arcu nibh et sed suscipit ipsum. Imperdiet congue dis mi pretium. Interdum egestas varius quis fames tortor.
                </div>              
              </div>

              <div class="tab">
                <input type="checkbox" id="chck5" />
                <label class="tab-label" for="chck5">Id mattis dui id cursus. Pretium pellentesque sed proin morbi sed commodo nunc pulvinar. At fusce praesent ac enim lectus tincidunt donec nisl, pharetra. Tellus sit in cursus a blandit posuere.</label>
                <div class="tab-content">
                Risus etiam ipsum eu, posuere lectus viverra. Egestas massa quis penatibus sem et duis dolor. Praesent aliquet et justo vel volutpat, volutpat euismod ut lorem. Massa quis et ullamcorper etiam sit sed morbi mi. Tortor vehicula cursus arcu nibh et sed suscipit ipsum. Imperdiet congue dis mi pretium. Interdum egestas varius quis fames tortor.
                </div>              
              </div>

            </div>

            <div class="duvidas">
              <p>Não encontrou o que precisava? <a href="#" >Entre em contato conosco</a>.</p>
            </div>

        </div>
      
      </div>

     </div>

        <Footer />

        <GoToTop />
  
      </>
      
    );
  }
  
  export default Faq;