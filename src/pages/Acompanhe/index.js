import React, { useState, useEffect } from "react";
import { useHistory, useParams, Link } from "react-router-dom";
import Axios from "axios";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { useForm } from "react-hook-form";

import Header from '../../components/Header';
import Footer from '../../components/Footer';
import Breadcrumb from "../../components/Breadcrumb";

import { faShoppingCart, faMapMarkerAlt, faFileAlt, faCheckCircle } from "@fortawesome/free-solid-svg-icons";
import { faCircle } from "@fortawesome/free-regular-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import Linha from "../../assets/images/etapaum.png";

import './acompanhe.css';

const Acompanhe = () => {

    const [order, setOrder] = useState({});
    const [orderAddress, setOrderAddress] = useState({});
    const [orderUser, setOrderUser] = useState({});
    const [orderProducts, setOrderProducts] = useState([]);
    const [orderDate, setOrderDate] = useState('');
    let { _id } = useParams();

    const finalDate = orderDate.toString().slice(0, 10).split('-').reverse().join('/');
    const paymentDate = orderDate.toString().slice(0, 10).split('-').reverse().join('/');
    
    function dateAddDays( /*string dd/mm/yyyy*/ datstr, /*int*/ ndays){
        var dattmp = datstr.split('/').reverse().join('/');
        var nwdate =  new Date(dattmp);
        nwdate.setDate(nwdate.getDate() + (ndays || 1));
        return [ zeroPad(nwdate.getDate(), 10)
                ,zeroPad(nwdate.getMonth()+1, 10)
                ,nwdate.getFullYear() ].join('/');
    }
    
    //function to add zero to date/month < 10
    function zeroPad(nr, base){
        var len = (String(base).length - String(nr).length) + 1;
        return len > 0? new Array(len).join('0') + nr : nr;
    }
    
    const finalPaymentDate = dateAddDays(paymentDate, +10).split('/').reverse().join('-')

    //const [ iuguPayment, setIuguPayment ] = useState();

    const [orderProductsIugu, setOrderProductsIugu] = useState([]);

    const { handleSubmit } = useForm();
    const history = useHistory();

    const [storesOrder, setStoresOrder] = useState([]);
    const [ApprovedProposal, setApprovedProposal] = useState([]);
    const [DeliveryPrice, setDeliveryPrice] = useState();
    const [StoreIuguAccount, setStoreIuguAccount] = useState();

    const totalValue = orderProducts.reduce((a, b) => +a + +b.price, 0);
    const centsTotalValue = totalValue.toFixed(1) *100;

    function paymentForm() {
        
        Axios.post( process.env.REACT_APP_API_BASE_URL + "/iugu/new_invoice",
            {
                "ensure_workday_due_date": false,
                "items": orderProductsIugu,
                "payable_with": [
                    "all"
                ],
                "payer": {
                    "address": {
                        "zip_code": orderAddress.cep,
                        "street": orderAddress.street,
                        "number": orderAddress.number,
                        "district": orderAddress.district,
                        "city": orderAddress.city,
                        "state": orderAddress.state,
                        "country": "Brasil",
                        "complement": orderAddress.complement
                    },
                    "cpf_cnpj": orderUser.cpf,
                    "name": orderUser.name,
                    "email": orderUser.email
                },
                "splits": [
                    {
                        "cents": centsTotalValue,
                        "recipient_account_id": StoreIuguAccount}
                ],
                "email": orderUser.email,
                "due_date": finalPaymentDate,
                "return_url": "https://clicklig.com.br/",
                "per_day_interest": false,
                "customer_id": "",
                "ignore_due_email": false,
                "order_id": 12345
            }
        )
        .then((res) => {
            console.log(res.data.secure_url);
            const token = JSON.parse(localStorage.getItem('token'));

            let axiosConfig = {
                headers: {
                'authorization': 'Bearer ' + token
                }
            };
            
            Axios.put( process.env.REACT_APP_API_BASE_URL + "/orders/add_payment/" + _id , {
                payment_url: res.data.secure_url
            }, axiosConfig)
        })
        .then((res) => { 
            window.open(res.data.secure_url, '_blank');
        })
        .catch((error) => {
            console.log(error.response.data.error);
            toast.error('😲' + error.response.data.error)
        });
  
      };

    useEffect(() => {
    
        const token = JSON.parse(localStorage.getItem('token'));
    
        const fetchOrder = async () => {
          const result = await Axios.get( process.env.REACT_APP_API_BASE_URL + "/orders/single/" + _id, {
            headers: {
              'authorization': 'Bearer ' + token
            }
          });
          setOrder(result.data);
          setOrderAddress(result.data.address);
          setOrderProducts(result.data.products);
          setOrderUser(result.data.user);
          setOrderDate(result.data.createdAt);        
        };
     
        fetchOrder();

        //

        const order_stores = Array.from(orderProducts.reduce(
          (m, {store, price}) => m.set(store, (m.get(store) || 0) + price *100), new Map
        ), ([store, price]) => ({recipient_account_id: store, cents: price}));
        setStoresOrder(order_stores);

        //

        const fetchApprovedProposal = async () => {
            const result = await Axios.get( process.env.REACT_APP_API_BASE_URL + "/orders/single/" + _id, {
                headers: {
                'authorization': 'Bearer ' + token
                }
            });

            const proposal = result.data.proposal.filter(x => x.status === "approved").map(x => x.products);
            const delivery_price = result.data.proposal.filter(x => x.status === "approved");
            
            if (proposal.length > 0) {
                setOrderProducts(proposal[0]);
                setDeliveryPrice(delivery_price[0].delivery_price);
                setStoreIuguAccount(delivery_price[0].iugu_id);
                setOrderProductsIugu(proposal[0].map((item) => ({
                    "description" : item.name + ' - ' + item.quantity + item.unity,
                    "quantity": '1',
                    "price_cents": item.price * 100
                })));
            }

        };
        
        fetchApprovedProposal();
    
    },[]);

    return (
  
      <>
  
        <Header />

      <div id="main-internas">
        
        <Breadcrumb />

        <div class="content etapas">

        <h1 className="brandtitle">Acompanhe seu pedido</h1>

        <div class="etapas-linha">

            {/* <div class="linha"><img class="banner" src={Linha} /></div> */}

            {order.status === 'order' ? 
                <ul>
                    <li class="concluido"><FontAwesomeIcon icon={faCheckCircle} /> Pedido Enviado</li>
                    <li><FontAwesomeIcon icon={faCircle} /> Orçamento Recebido</li>
                    <li><FontAwesomeIcon icon={faCircle} /> Pagamento Realizado</li>
                </ul>
            : null}

            {order.status === 'proposal' ? 
                <ul>
                    <li class="concluido"><FontAwesomeIcon icon={faCheckCircle} /> Pedido Enviado</li>
                    <li class="concluido"><FontAwesomeIcon icon={faCheckCircle} /> Orçamento Recebido</li>
                    <li><FontAwesomeIcon icon={faCircle} /> Pagamento Realizado</li>
                </ul>
            : null}

            {order.status === 'payment' ? 
                <ul>
                    <li class="concluido"><FontAwesomeIcon icon={faCheckCircle} /> Pedido Enviado</li>
                    <li class="concluido"><FontAwesomeIcon icon={faCheckCircle} /> Orçamento Recebido</li>
                    <li class="concluido"><FontAwesomeIcon icon={faCheckCircle} /> Pagamento Realizado</li>
                </ul>
            : null}

            
        </div>

        <div class="details-pedido">

            <div class="left">

                <div class="details box">

                    <h4 class="title"><i class="fas"><FontAwesomeIcon icon={faShoppingCart} /></i> Detalhes do pedido</h4>

                    <ul>
                        <li>Data: {finalDate}</li>
                        <li>Identificação: {order._id}</li>    
                    </ul>

                </div>

                <div class="address box">

                    <h4 class="title">
                        <i class="fas"><FontAwesomeIcon icon={faMapMarkerAlt} /></i> Endereço
                    </h4>

                    <ul>
                        <li>{orderAddress.name}</li>
                        <li>{orderAddress.street} - {orderAddress.complement}</li>
                        <li>{orderAddress.district} | CEP: {orderAddress.cep}</li>
                        <li>{orderAddress.city} / {orderAddress.state}</li>
                    </ul>

                </div>

            </div>

            <div class="right">

                <div class="description">

                    <h3><FontAwesomeIcon icon={faFileAlt} /> Lista de Itens</h3>

                    <ul>
                        {/* { ApprovedProposal === null && <li>Menor q zero</li> } */}
                        {/* {ApprovedProposal.length > 0 &&
                            <>
                            {ApprovedProposal.map((item, idx) => (
                                <li class="row" key={idx}>
                                    <span class="category">{idx + 1}. {item.name}</span>
                                    <span class="product">{item.category}</span>
                                    <span class="units">{item.quantity} {item.unity}</span>
                                    <span class="price">R$ {item.price}</span>
                                </li>
                            ))}
                            </>
                        }
                        {ApprovedProposal.length = 0 &&
                            <>
                            
                            </>
                        } */}
                        {orderProducts.map((item, idx) => (
                            <li class="row" key={idx}>
                                <span class="category">{idx + 1}. {item.name}</span>
                                <span class="product">{item.category}</span>
                                <span class="units">{item.quantity} {item.unity}</span>
                                <span class="price">R$ {item.price}</span>
                            </li>
                        ))}
                    </ul>

                    {ApprovedProposal != null ? 
                        <p className="finalprice">R$ {totalValue.toFixed(2)}<span>Frete: R$ {DeliveryPrice}</span></p>
                    : <p>a</p> }

                    
                    {/* <span className={order.type}></span>
                    <p className="pdflink"><Link to="#">Abrir PDF com detalhes</Link></p> */}

                    <div className="clear"></div>

                </div>

                { order.status === 'proposal' ? 
                    <>
                        {order.payment_url != null ?
                            <><br/> 
                            <a href={order.payment_url} target="_blank" className="btn green">Realizar pagamento</a></>
                        : <form className="criar-pedido" onSubmit={handleSubmit(paymentForm)}>
                            <button className="btn green">Realizar pagamento</button> 
                        </form> }    
                    </>               
                    : <span></span>
                }

                <div className="clear"></div>

            </div>


        </div>
      
      </div>

     </div>

        <ToastContainer />

        <Footer />
  
      </>
      
    );
  }
  
  export default Acompanhe;