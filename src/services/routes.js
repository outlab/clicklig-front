import React from 'react';
import { BrowserRouter, Redirect, Switch, Route } from 'react-router-dom';

import { isAuthenticated } from './auth';

import Home from './../pages/Home';
import Privacy from '../pages/Privacy';
import Terms from '../pages/Terms';
import Faq from '../pages/Faq';
import CriarConta from '../pages/CriarConta';
import Login from '../pages/Login';
import TroqueSenha from '../pages/ForgotPassword';
import ChangePass from '../pages/ChangePass';
import PedidoConfirmado from '../pages/PedidoConfirmado';
import EtapaUm from '../pages/Acompanhe';
import Contato from '../pages/Contato';
import Acompanhe from '../pages/Acompanhe';
import AcompanheDois from '../pages/AcompanheDois';
import AcompanheTres from '../pages/AcompanheTres';
import AcompanheFim from '../pages/AcompanheFim';
import MeusDados from '../pages/MeusDados';
import Enderecos from '../pages/MeusEnderecos';
import Order from '../pages/CriarPedido';
import MyOrders from '../pages/MeusPedidos';
import Fornecedor from '../pages/Fornecedor';


//import '../assets/style.css';

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route {...rest} render={props => 
      isAuthenticated() ? (
        <Component {...props} />
      ) : (
        <Redirect to={{ pathname: "/login", state: { from: props.location } }} />
      )
    }
  />
);

function Routes() {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/politica-de-privacidade" component={Privacy} />
        <Route path="/termos-e-condicoes" component={Terms} /> 
        <Route path="/faq" component={Faq} />        
        <Route path="/criar-conta" component={CriarConta} />
        <Route path="/login" component={Login} />
        <Route path="/esqueci-a-senha" component={TroqueSenha} />
        <Route path="/trocar-senha" component={ChangePass} />
        {/* <Route path="/acompanhe-seu-pedido-dois" component={AcompanheDois} />
        <Route path="/acompanhe-seu-pedido-tres" component={AcompanheTres} />
        <Route path="/acompanhe-seu-pedido-quatro" component={AcompanheFim} /> */}
        <Route path="/fale-conosco" component={Contato} />
        <Route path="/seja-um-fornecedor" component={Fornecedor} />
        <PrivateRoute path="/meus-enderecos" component={Enderecos} />
        <PrivateRoute path="/meus-dados" component={MeusDados} />
        <PrivateRoute path="/meus-pedidos" component={MyOrders} />
        <PrivateRoute path="/fazer-pedido" component={Order} />
        <PrivateRoute path="/pedido-confirmado" component={PedidoConfirmado} />
        <PrivateRoute path="/acompanhe-seu-pedido/:_id" label="Acompanhe seu Pedido" component={Acompanhe} />
      </Switch>
    </BrowserRouter>
  );
}

export default Routes;